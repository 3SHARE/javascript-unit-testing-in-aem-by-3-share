# Javascript Unit testing in AEM

This is an example project to illustrate how we can achieve Frontend JS unit testing in an AEM environment. 

## How to build

To build all the modules run in the project root directory the following command with Maven 3:

    mvn clean install

To build all the modules and deploy the `all` package to a local instance of AEM, run in the project root directory the following command:

    mvn clean install -PautoInstallSinglePackage

Or to deploy it to a publish instance, run

    mvn clean install -PautoInstallSinglePackagePublish

Or alternatively

    mvn clean install -PautoInstallSinglePackage -Daem.port=4503

Or to deploy only the bundle to the author, run

    mvn clean install -PautoInstallBundle

Or to deploy only a single content package, run in the sub-module directory (i.e `ui.apps`)

    mvn clean install -PautoInstallPackage

## Running the FE Unit tests as part of maven build

The unit tests will all run as part of the maven build causing the build to break on failure. Specifically, within the `ui.frontend` 
module's `package.json` it will run the `prod` script. Which in-turn calls the `test` script. If there are tests written and 
there is a failure, it will cause the maven build to fail, though as we are passing the `--passWithNoTests` flag will continue as normal 
if there are no unit tests written.

## Running the FE Unit tests during development

For the following commands you must ensure you are inside the `ui.frontend` directory. For the best results please use NodeJS v14.16.0+

To install all required packages:

    npm install

To run all the tests:

    npm run test

During development or using TDD approach, you would likely want to watch the tests as you are working on them, for this you can run:

    npm run test-watch

If you would like to see the code coverage for the tests written:

    npm run test-coverage
