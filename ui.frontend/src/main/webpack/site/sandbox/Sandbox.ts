const SELECTORS = {
	SANDBOX_COMPONENT: '[data-cmp-is="sandbox"]',
	SANDBOX_COMPONENT_CONTAINER: '.cmp-sandbox',
	BUTTON: '.btn-update',
}

class Sandbox {
	$el: HTMLElement;
	$button: HTMLButtonElement;

	/**
	 * @constructor
	 * @param {HTMLElement} $el - Component parent element
	 * */
	constructor($el) {
		this.$el = $el;
		this.$button = this.$el.querySelector(SELECTORS.BUTTON);
	}

	/**
	 * addListeners: Listener to show updated text when button is clicked
	 * */
	private addListeners(): void {
		this.$button.addEventListener('click', ()=>{
			this.$button.innerHTML = 'updated text';
		})
	}

	init(): void {
		this.$el.removeAttribute('data-cmp-is');
		this.addListeners();
	}
}

const sandboxEls: NodeListOf<HTMLElement> = document.querySelectorAll(SELECTORS.SANDBOX_COMPONENT);

if (sandboxEls.length) {
	sandboxEls.forEach(($el: HTMLElement) => {
		const sandbox = new Sandbox($el);
		sandbox.init();
	})
}
