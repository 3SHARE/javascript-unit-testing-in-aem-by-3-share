import {SELECTORS, fixture} from "./sandbox-fixture";

describe("Sandbox", () => {
    afterEach(() => {
		document.body.innerHTML = "";
	});
    
    it('ensures component is available in DOM', ()=>{
        document.body.innerHTML = fixture;

		require('../../src/main/webpack/site/sandbox/Sandbox');
        const $container: HTMLElement = document.querySelector(SELECTORS.SANDBOX_COMPONENT_CONTAINER);
        expect($container).not.toBeNull();
    });
    
    it('gracefully fails when component not available in DOM', ()=>{

		require('../../src/main/webpack/site/sandbox/Sandbox');
        const $container: HTMLElement = document.querySelector(SELECTORS.SANDBOX_COMPONENT_CONTAINER);
        expect($container).toBeNull();
    });
    
    it("updates button text on click", () => {
        document.body.innerHTML = fixture;

		require('../../src/main/webpack/site/sandbox/Sandbox');
        const $button: HTMLButtonElement = document.querySelector(SELECTORS.BUTTON);
        
        expect($button.innerHTML).toEqual('default button text');
        expect($button.innerHTML).not.toEqual('updated text');
        
        $button.click();
        
        expect($button.innerHTML).not.toEqual('default button text');
        expect($button.innerHTML).toEqual('updated text');
    });
});
