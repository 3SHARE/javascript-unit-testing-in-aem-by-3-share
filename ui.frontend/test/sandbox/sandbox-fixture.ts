export const SELECTORS = {
	SANDBOX_COMPONENT: '[data-cmp-is="sandbox"]',
    SANDBOX_COMPONENT_CONTAINER: '.cmp-sandbox',
	BUTTON: '.btn-update',
}

export const fixture = `<div class="cmp-sandbox" data-cmp-is="sandbox">
	<button class="btn-update">default button text</button>
</div>`;
