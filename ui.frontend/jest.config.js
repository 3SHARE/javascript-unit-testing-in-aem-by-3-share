module.exports = {
	clearMocks: true,
	setupFilesAfterEnv: ['<rootDir>/test/setupTests.ts'],
	collectCoverageFrom: ['src/main/webpack/site/**/*.{ts,js,jsx,mjs}'],
	testEnvironment: 'jsdom'
};
